# TextLayoutBuilder

## 项目简介

本库基于[TextLayoutBuilder](https://github.com/facebook/TextLayoutBuilder) 原库进行适配,使其可以运行在 OpenHarmony。TextLayoutBuilder是一个可定制任意样式的文本构建工具，包括字体间距、大小、颜色、布局方式、富文本高亮显示等，在文字显示的业务场景中都会使用到，特别是通知类特殊显示的字体样式时，TextLayoutBuilder极大的方便了开发者的开发效率。

## 效果演示
![gif](preview.gif)

## 下载安装

 ```
 ohpm install @ohos/textlayoutbuilder 
 ```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)


## 使用说明：

第一步： 初始化，导入 TextLayout 组件到自己项目中，实例化TextLayout.Layout对象；
```typescript
...
import TextLayout from '@ohos/textlayoutbuilder';
import {TextInfo} from '@ohos/textlayoutbuilder';
...
@State layout:TextLayout.Layout = new TextLayout.Layout(); 
...

```
第二步：属性设置，通过layout类对象设置UI属性来自定义所需风格，也可以添加所需的回调；
```typescript
private aboutToAppear() {

    let textInfo = new TextInfo();
    textInfo.setStart(2)
    textInfo.setEnd(8)
    textInfo.setFontColor('#ff0000')
    ... ...

    this.layout
    .setText(this.strTest)
    .setSpecialTextInfo(textInfo1)
    .setSpecialTextInfo(textInfo2)
    .setSpecialTextInfo(textInfo3)
    .setSpecialTextClick((textInfo) =>{
    console.info('我点击了 = '+textInfo.getText())
    });
}
```
第三步：界面绘制，将定制好的layout传给TextLayout。
```typescript
build() {
    Column() {
         Text("默认显示").fontSize(16).fontColor("#999999").margin({ left: 14, top: 14, bottom: 14 })
         Column() {
            TextLayout({ model: this.layout })
         }.backgroundColor("#cccccc").margin({ left: 14, right: 14 }).borderRadius(10)
    }.alignItems(HorizontalAlign.Start)
}
```
## 接口说明
```
let layout:TextLayout.Layout = new TextLayout.Layout();
```
1. 设置文字内容：layout.setText(text: string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   |  text       | string    | 是   | 文本内容    |

2. 设置指定文字内容样式：layout.setSpecialTextInfo(specialTextInfo: TextInfo)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   |  specialTextInfo  | TextInfo    | 是   | 文本内容样式 |

3. 设置文字颜色：layout.setTextColor(textColor: string|Color|number)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   |  textColor    | string&#124;Color&#124;number | 是   | 文本颜色    |

4. 设置是否单行显示：layout.setSingleLine(singleLine: boolean)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | singleLine    | boolean    | 是   | 是否单行显示，默认否    |

5. 设置最大行数：layout.setMaxLines(maxLines: number)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   |   maxLines   | number   | 是   | 最大行数    |

6. 设置文本显示不下时的省略号替代方式：layout.setEllipsize(ellipsize: TextOverflow)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   |ellipsize   | TextOverflow | 是   | 文本溢出显示方式    |
  
7. 设置文本对齐方式：layout.setAlignment(textalign: TextAlign)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | textalign  | TextAlign | 是   | 文本对齐方式    |

8. 设置每行最小显示字数：layout.setMinEms(minEms: number)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   |minEms     | number | 是  | 每行显示字数  |

9. 设置组件是否设置内边距：layout.setIncludeFontPadding(shouldInclude: boolean)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | shouldInclude | boolean   | 是   | 是否设置padding，默认否 |

10. 设置最大宽度：layout.setMaxWidth(maxWidth: number)

    **参数：**

    | 参数名       | 参数类型    | 必填  |参数描述|
    | :---------- | ----------| -----|-----  |
    | maxWidth | number  | 是  | 最大宽度 |


11. 设置是否开启按下文字时状态变化开关：layout.setIsEnablePressState(isEnablePressState: boolean)

    **参数：**

    | 参数名       | 参数类型    | 必填  |参数描述|
    | :---------- | ----------| -----|-----  |
    | isEnablePressState  | boolean | 是 | 是否开启按下文字时状态变化开关，默认否 |

12. 设置按下文字状态样式：layout.setTextPressStateStyle(textPressStateStyle: string)

    **参数：**

    | 参数名       | 参数类型    | 必填  |参数描述|
    | :---------- | ----------| -----|-----  |
    |textPressStateStyle | string | 是   | 按下文字状态样式    |

13. 设置指定文本的点击事件：layout.setSpecialTextClick(clickCallback: (textInfo: TextInfo) => void)

    **参数：**

    | 参数名       | 参数类型    | 必填  |参数描述|
    | :---------- | ----------| -----|-----  |
    | clickCallback | (textInfo: TextInfo) => void   | 是  | 用户自定义回调函数    |

```
特殊字符属性设置： let textInfo = new TextInfo();
```
1. 设置特殊文字开始位置：textInfo.setStart(start:number)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | start  | number | 是  | 特殊文字开始位置  |

2. 设置特殊文字结束位置：textInfo.setEnd(end:number)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | end | number  |是  | 特殊文字结束位置  |

3. 设置文字颜色：textInfo.setFontColor(fontColor:string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | fontColor   | string    | 是 | 文字颜色   |

4. 设置文字内容：textInfo.setText(text:string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | text      | string  |是   | 文本内容    |

5. 设置文字类型：textInfo.setTextType(textType:string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | textType     | string | 是   |文本类型（TEXT_TYPE_NORMAL&#124;TEXT_TYPE_RICH&#124;TEXT_TYPE_HTTP）   |

6. 设置文本字体之间的距离：textInfo.setFontLetterSpacing(fontLetterSpacing:string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | fontLetterSpacing  | string | 是   |文本字体之间的距离  |

7. 设置字体大小：textInfo.setFontSize(fontSize:string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | fontSize    | string  | 是  | 字体大小 |

8. 设置字体样式：textInfo.setFontStyle(fontStyle:string)

   **参数：**

   | 参数名       | 参数类型    | 必填  |参数描述|
   | :---------- | ----------| -----|-----  |
   | fontStyle  | string  | 是  | 字体样式 |

## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本：3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## 目录结构

```
|---- TextLayoutBuilder
|     |---- entry  # 示例代码文件夹
|     |---- library  # TextLayout库文件夹
|          |---- src
|            |---- main
|              |---- ets
|                  |---- commonents
|                       |---- TextInfo.ets  # 富文本数据实体
|                       |---- TextLayout.ets  # UI 自定义组件
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/TextLayoutBuilder/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/TextLayoutBuilder/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/TextLayoutBuilder/blob/master/LICENSE) 协议，请自由地享受和参与开源。
